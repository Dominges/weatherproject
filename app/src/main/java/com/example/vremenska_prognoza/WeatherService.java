package com.example.vremenska_prognoza;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.FileDescriptor;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class WeatherService extends Service { // Kada extendujemo Serive automacki generisemo metodu onBind

    private final IBinder mBinder;
    private String mName;
    private String mURL;

    public WeatherService() {
        mBinder = new LocalBinder();
        //mTimer = new Timer();
        //mUpdateTask = new UpdateWeatherTask();
        //mUpdateTask.setPointer(this);

        //mTimer.scheduleAtFixedRate(mUpdateTask, mDelay, mPeriod);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        mName = "";
        mURL = "http://api.openweathermap.org/data/2.5/forecast/daily?q=Novi%20Sad,RS&mode=json&units=metric&cnt=5";
        return mBinder;


    }

    void getWeatherInfo(String name) { //U ovoj metodi getWeatherInfo za odredjeni grad
        String text = "";
        if(name.equals(""))
            mName = "";
        else
            mName = name;

        mURL = "api.openweathermap.org/data/2.5/forecast?q=" +name+ ",RS&APPID=d72964f5d0bb05bcce616ad5bdae122e"; //moj API key
        mURL = mURL.replaceAll(" ", "%20"); //menja spejs u %20
        //Log.d(TAG, "Weather for city " + mName + " is fetched.");
        // Create http cliient object to send request to server

        URL url = null;
        try {
            url = new URL(mURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection client = null;
        try {
            client = (HttpURLConnection) url.openConnection();
        }
        catch (IOException e) {
            e.printStackTrace();
        }


        Toast.makeText(getApplicationContext(),
                "ghjgjgujg", Toast.LENGTH_SHORT).show();
        try {
            client.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }



        try {
            text = client.getResponseMessage();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Toast.makeText(getApplicationContext(),
                "Stigli su podaci " + text, Toast.LENGTH_SHORT).show();


    }

    public class LocalBinder implements IBinder {
        WeatherService getService() {
            //Log.d(TAG, "Service is created and attempt to connect.");
            return WeatherService.this;
        }

        @Nullable
        @Override
        public String getInterfaceDescriptor() throws RemoteException {
            return null;
        }

        @Override
        public boolean pingBinder() {
            return false;
        }

        @Override
        public boolean isBinderAlive() {
            return false;
        }

        @Nullable
        @Override
        public IInterface queryLocalInterface(@NonNull String descriptor) {
            return null;
        }

        @Override
        public void dump(@NonNull FileDescriptor fd, @Nullable String[] args) throws RemoteException {

        }

        @Override
        public void dumpAsync(@NonNull FileDescriptor fd, @Nullable String[] args) throws RemoteException {

        }

        @Override
        public boolean transact(int code, @NonNull Parcel data, @Nullable Parcel reply, int flags) throws RemoteException {
            return false;
        }

        @Override
        public void linkToDeath(@NonNull DeathRecipient recipient, int flags) throws RemoteException {

        }

        @Override
        public boolean unlinkToDeath(@NonNull DeathRecipient recipient, int flags) {
            return false;
        }
    }
}
