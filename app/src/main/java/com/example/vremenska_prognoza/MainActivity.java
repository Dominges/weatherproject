package com.example.vremenska_prognoza;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class MainActivity extends AppCompatActivity implements WatherInterface {
    Button showWeather;
    TextView cityName;
    WatherInterface mSelf;
    String mURL="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prepareElements();
        prepareButton();
    }

    private void prepareElements(){
        showWeather = findViewById(R.id.ButtonShowWeather);
        cityName = findViewById(R.id.TextCityName);
    }

    private void prepareButton() {
        showWeather.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String text = cityName.getText().toString();
                Toast.makeText(getApplicationContext(),
                        "Izabrali ste grad " + text, Toast.LENGTH_SHORT).show();

                if(!cityName.equals(text)) { //Kad pristisnemo dugme ShowWeather ime buttona ce se promeniti u Refresh
                    cityName.setText(text);
                    //mBoundService.getWeatherInfo(mCity);
                    //Log.d(TAG, "Weather service is running and we wait for data.");
                    showWeather.setText("Refresh");
                } else {
                    //mBoundService.refreshWeather();
                }

                mURL = "http://api.openweathermap.org/data/2.5/forecast?q=" +text+ ",RS&APPID=d72964f5d0bb05bcce616ad5bdae122e"; //moj API key
                mURL = mURL.replaceAll(" ", "%20"); //menja spejs u %20
                //Log.d(TAG, "Weather for city " + mName + " is fetched.");
                // Create http cliient object to send request to server

                URL url = null;
                try {
                    url = new URL(mURL);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection client = null;
                try {
                    client = (HttpURLConnection) url.openConnection();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    client.connect();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    text = client.getResponseMessage();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Toast.makeText(getApplicationContext(),
                        "Stigli su podaci " + text, Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void fetchWeatherData() {

    }
}
